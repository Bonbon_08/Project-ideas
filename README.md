# Project-ideas
## Finished
- [x] .desktop file creator
- [x] own texteditor
- [x] own simple game
## Current projects
- [ ] own operating system
- [ ] own music player
- [ ] own linux distro
## Future projct ideas
- own packet manager
- own c64 or amiga emulator
- designing an own architecture
- writing a chatbot
- creating a chatprogramm
- building a browser
- own file format
- custom firmware for some elektornics
- create an own gameengine
